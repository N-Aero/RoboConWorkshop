def task_run_tests():
    return {
        'actions':['poetry run robot -A jedi.testconfig jedi_tests'],
        'verbosity': 0
    }
