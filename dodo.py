from doit.tools import create_folder


def task_serve_packages():
    "start local pypi"
    path = "local-repository"
    return {
        "actions": [
            (create_folder, [path]),
            f"pypi-server run -a . -P . -p 9000 --overwrite {path}"
        ]
    }


def task_hello():
    print("hello")
